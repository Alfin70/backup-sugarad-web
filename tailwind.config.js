module.exports = {
  theme: {
    extend: {
      fontFamily: {
        sans: [
          "Rubik",
          "Roboto",
          "-apple-system",
          "BlinkMacSystemFont",
          "Segoe UI",
          "Helvetica Neue",
          "Arial",
          "Noto Sans",
          "sans-serif",
          "Apple Color Emoji",
          "Segoe UI Emoji",
          "Segoe UI Symbol",
          "Noto Color Emoji"
        ]
    
      },
      colors: {
        aqua: {
          100: "#EDFAFB",
          200: "#D1F2F4",
          300: "#B5E9ED",
          400: "#7ED9E0",
          500: "#46C9D2",
          600: "#3FB5BD",
          700: "#2A797E",
          800: "#205A5F",
          900: "#153C3F"
        },
        yellow: {
          100: "#FFF9EB",
          200: "#FEF0CD",
          300: "#FDE6AF",
          400: "#FCD473",
          500: "#FAC137",
          600: "#E1AE32",
          700: "#967421",
          800: "#715719",
          900: "#4B3A11"
        },
        gray: {
          100: "#FEFEFE",
          200: "#FDFDFD",
          300: "#FCFCFC",
          400: "#FAFAFA",
          500: "#F8F8F8",
          600: "#DFDFDF",
          700: "#959595",
          800: "#707070",
          900: "#4A4A4A"
        },
        green: {
          100: "#EFFAF6",
          200: "#D7F4E8",
          300: "#BFEDDA",
          400: "#90DFBF",
          500: "#60D1A3",
          600: "#56BC93",
          700: "#3A7D62",
          800: "#2B5E49",
          900: "#1D3F31"
        },
        red: {
          100: "#FDF0EF",
          200: "#FBD9D6",
          300: "#F9C2BD",
          400: "#F4958C",
          500: "#EF675A",
          600: "#D75D51",
          700: "#8F3E36",
          800: "#6C2E29",
          900: "#481F1B"
        }
      },
      
     
    }
  },
  variants: {
    textColor: ['responsive', 'hover', 'focus', 'group-hover', 'active'],
 
  },
  plugins: []
};
